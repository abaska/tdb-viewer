package model;

import java.util.ArrayList;

public class DataTable {
	private ArrayList<String> columns;
	private ArrayList<ArrayList<String>> data;
	
	public DataTable(){
		columns = new ArrayList<>();
		data = new ArrayList<ArrayList<String>>();
	}
	
	public ArrayList<String> getColumns() {
		return columns;
	}
	
	public void setColumns(ArrayList<String> columns) {
		this.columns = columns;
	}
	
	public void addColumn(String column) {
		this.columns.add(column);
	}
	
	public void removeColumn(String column) {
		this.columns.remove(columns.indexOf(column));
	}
	
	public ArrayList<ArrayList<String>> getRows() {
		return data;
	}
	
	public void setRows(ArrayList<ArrayList<String>> rows) {
		this.data = rows;
	}
	
	public void newRow() {
		this.data.add(new ArrayList<String>());
	}
	
	public void addElement(String elem){
		this.data.get(data.size()-1).add(elem);
	}
	
}
