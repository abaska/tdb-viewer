package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb.TDBFactory;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/helloServlet")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String entity = request.getParameter("entity");
		Dataset ds = TDBFactory.createDataset((String)request.getParameter("dataSetName"));
		ds.begin(ReadWrite.WRITE);
		Model model = ds.getNamedModel(entity);
		PrintWriter out = response.getWriter();
		try (QueryExecution qExec = QueryExecutionFactory.create("SELECT * WHERE{?S ?P ?O}", model)) {	// instead of query it was qs3
			ResultSet rs = qExec.execSelect();
			out.println("<html><body><table border='1'>" + 
					"<tbody><tr><th>Subject</th><th>Object</th><th>Predicate</th></tr>"); 
					
			while (rs.hasNext()) {
				QuerySolution querySolution = rs.next();
				out.println("<tr>" + 
				"<td>"+querySolution.get("?S").toString() +"</td>" + 
				"<td>"+querySolution.get("?P").toString() +"</td>" + 
				"<td>"+querySolution.get("?O").toString() +"</td>" + 
				"</tr>");
				
			}
			out.println("</tbody></table></body></html>");
			
			ds.close();	
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
			
		
		//request.setAttribute("name", yourName);
	//	request.getRequestDispatcher("index.jsp").forward(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String yourName = request.getParameter("entity");
		request.setAttribute("name", yourName);
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

}
