package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import model.DataTable;
import model.Row;

/**
 * Servlet implementation class LoadValues
 */
@WebServlet("/LoadValues2")
public class LoadValues2 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadValues2() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ArrayList<Row> triples;
		String result = "";
		try {
			if (request.getParameter("hasQuery") == null) {
				triples = new DataManager().getDatabase(request.getParameter("entity"));
			} else {
				triples = new DataManager().getDatabaseByQuery(request.getParameter("entity"),
						request.getParameter("query"));
			}
			
			if (triples != null) {
				result = parseResultArray(triples);
			}

			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(result);
			
		} catch (Exception e) {
			response.setContentType("text/plain"); 
			response.setCharacterEncoding("UTF-8");
			response.sendError(400, "error");
			e.printStackTrace();
		}

	}
	
	private String parseResultMap(ArrayList<Row> par){
		Map<String, HashMap<String, String>> res = new HashMap<String, HashMap<String, String>>();
		
		for (Row row : par) {
			String s = trimURI(row.getSubject());
			String p = trimURI(row.getPredicate());
			String o = trimURI(row.getObject());
			
			if (!res.containsKey(s))
				res.put(s, new HashMap<String, String>());
			res.get(s).put(p, o);
		}
		
		return new Gson().toJson(res);
	}
	
	private String parseResultArrayMap(ArrayList<Row> par){
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		
		Map<String, HashMap<String, String>> map = new HashMap<String, HashMap<String, String>>();
		
		for (Row row : par) {
			String s = trimURI(row.getSubject());
			String p = trimURI(row.getPredicate());
			String o = trimURI(row.getObject());
			
			if (!map.containsKey(s))
				map.put(s, new HashMap<String, String>());
			map.get(s).put(p, o);
		}
		
		for (Map.Entry<String, HashMap<String, String>> entry : map.entrySet())
		{
		    result.add(entry.getValue());
		}
		
		String res = new Gson().toJson(result);
		System.out.println(res);
		return res;
	}
	
	private String parseResultArray(ArrayList<Row> par){
		DataTable dt = new DataTable();
		
		int prevS = 0;
		
		for (Row triple : par) {
			String s = trimURI(triple.getSubject());
			int currS = Integer.parseInt(s);
			String p = trimURI(triple.getPredicate());
			String o = trimURI(triple.getObject());
			
			if (currS > prevS){
				dt.newRow();
				prevS = currS;
			}
			
			if (!dt.getColumns().contains(p))
				dt.addColumn(p);
			
			dt.addElement(o);
		}
		
		String res = new Gson().toJson(dt);
		System.out.println(res);
		return res;
	}

	private static String trimURI(Object s) {
		String h = (String) s;
		if(h.contains("#"))
			h = h.split("#")[1];
		
		return h;
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
