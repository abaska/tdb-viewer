package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.tdb.TDBFactory;

/**
 * Servlet implementation class GetEntity
 */
@WebServlet("/getEntity")
public class GetEntity extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetEntity() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	doGet(request, response);
		Dataset ds = TDBFactory.createDataset((String)request.getParameter("dataSetName"));
		ds.begin(ReadWrite.WRITE);
		request.setAttribute("dataSet", (String)request.getParameter("dataSetName"));
		ArrayList<String> entities = new ArrayList<String>();
		String queryStr = "SELECT ?g WHERE { GRAPH ?g { } }";
		try (QueryExecution qExec = QueryExecutionFactory.create(queryStr, ds)) { // instead of query it was qs3
			ResultSet rs = qExec.execSelect();
			while (rs.hasNext()) {
				// System.out.println(rs.next());
				entities.add(rs.next().get("g").toString());
			}

		ds.close();	
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		request.setAttribute("entities", entities);
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

}
