package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jena.atlas.logging.Log;

import com.google.gson.Gson;

import model.Row;

/**
 * Servlet implementation class LoadValues
 */
@WebServlet("/LoadValues")
public class LoadValues extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoadValues() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			if (request.getParameter("hasQuery") == null) {
				ArrayList<Row> triples = new DataManager().getDatabase(request.getParameter("entity"));

				response.setContentType("text/plain"); 

				response.setCharacterEncoding("UTF-8"); 
				if (triples != null) {
					String json = new Gson().toJson(triples);
					System.out.println(json);
					response.getWriter().write(json);
				}
			} else {
				ArrayList<Row> triples = new DataManager().getDatabaseByQuery(request.getParameter("entity"),
						request.getParameter("query"));

				response.setContentType("text/plain"); 

				response.setCharacterEncoding("UTF-8"); 
				if (triples != null) {
					String json = new Gson().toJson(triples);
					System.out.println(json);
					response.getWriter().write(json);
				}
			}
		} catch (Exception e) {
			response.setContentType("text/plain"); 
			response.setCharacterEncoding("UTF-8");
			response.sendError(400, "error");
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
