package servlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb.TDBFactory;

import model.Row;

public class DataManager {

	public ArrayList<String> getEntities() {
		Dataset ds = TDBFactory
				.createDataset("C:\\Program Files (x86)\\eclipse-jee-oxygen-R-win32-x86_64\\eclipse\\TDBsmaRT");
		ds.begin(ReadWrite.WRITE);
		ArrayList<String> entities = new ArrayList<String>();
		String queryStr = "SELECT ?g WHERE { GRAPH ?g { } }";
		try (QueryExecution qExec = QueryExecutionFactory.create(queryStr, ds)) { // instead of query it was qs3
			ResultSet rs = qExec.execSelect();
			while (rs.hasNext()) {
				// System.out.println(rs.next());
				entities.add(rs.next().get("g").toString());
			}

			ds.close();
		} catch (Exception e) {
			e.printStackTrace();
			ds.close();
			throw e;
		}

		return entities;
	}

	public ArrayList<Row> getDatabase(String entity) {
		ArrayList<Row> triples = new ArrayList<Row>();
		Dataset ds = TDBFactory.createDataset("TDBsmaRT");
		ds.begin(ReadWrite.WRITE);
		Model model = ds.getNamedModel(entity);

		try (QueryExecution qExec = QueryExecutionFactory.create("SELECT * WHERE{?S ?P ?O}", model)) { 
			ResultSet rs = qExec.execSelect();
			while (rs.hasNext()) {
				QuerySolution querySolution = rs.next();
				
				Row row = new Row();
				row.setSubject(querySolution.get("?S").toString());
				row.setPredicate(querySolution.get("?P").toString());
				row.setObject(querySolution.get("?O").toString());
				
				triples.add(row);
			}

			ds.close();

		}catch(Exception e) {
			throw e;
		}
		return triples;

	}
	
	public ArrayList<Row> getDatabaseByQuery(String entity,String query) {
		ArrayList<Row> triples = new ArrayList<Row>();
		Dataset ds = TDBFactory.createDataset("TDBsmaRT");
		ds.begin(ReadWrite.WRITE);
		Model model = ds.getNamedModel(entity);

		try (QueryExecution qExec = QueryExecutionFactory.create(query, model)) { 
			ResultSet rs = qExec.execSelect();
			while (rs.hasNext()) {
				QuerySolution querySolution = rs.next();
				
				Row row = new Row();
				row.setSubject(querySolution.get("?S").toString());
				row.setPredicate(querySolution.get("?P").toString());
				row.setObject(querySolution.get("?O").toString());
				
				triples.add(row);
			}

			ds.close();

		}catch(Exception e) {
			ds.close();
			throw e;
		}
		return triples;

	}

}
