

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Map.Entry"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src="//code.jquery.com/jquery-1.12.4.js"></script>
      <script
         src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
      <script
         src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
      <link rel="stylesheet" type="text/css"
         href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link rel="stylesheet" type="text/css"
         href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
      <title>TDB Viewer</title>
   </head>
   <body>
      <%if(request.getAttribute("entities")==null){%>
      <script type="text/javascript">
         window.location = "loadEntity";
      </script>
      <%}%>
      <div class="container">
         <div class="page-header">
            <h1>
               TDB Database viewer <small> Beta version</small>
            </h1>
         </div>
         <div class="row">
         	<div class="form-group">
    			<label for="exampleFormControlTextarea1">Execute a SPARQL query</label>
    			<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
  			</div>
  			<button type="button" onClick="runQuery()" class="btn btn-success pull-right">RUN</button>
         </div>
         <hr />
         <div class="row">
            <div class="col-md-2">
               <ul class="nav nav-pills nav-stacked">
                  <%if(request.getAttribute("entities")!=null){
                     ArrayList<String> entities =(ArrayList) request.getAttribute("entities");
                        for(int i = 0; i < entities.size(); i+=1) { %>
                  <li role="presentation" onClick="loadDoc('<%=entities.get(i) %>')"><a
                     href="#"><%=entities.get(i) %></a></li>
                  <% }}%>
               </ul>
            </div>
            <div class="col-md-10">
               <table id="example" class="table table-striped table-bordered" 	width="100%" cellspacing="0">
                  <thead>
                     <tr>
                        <th>Subject</th>
                        <th>Object</th>
                        <th>Predicate</th>
                     </tr>
                  </thead>
                  <tfoot>
                     <tr>
                        <th>Subject</th>
                        <th>Object</th>
                        <th>Predicate</th>
                     </tr>
                  </tfoot>
                  <tbody>
                    
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <script>
         $(document).ready(function() {
             $('#example').DataTable();
         } );
         
      </script>
      <script>
      var entity = "";
         function loadDoc(para) {
         entity = para;	
     
         
           $.ajax({
        	   url: "LoadValues",
        	   type: "get", //send it through get method
        	   data: { 
        		 entity: para
        	    },
        	   success: function(response) {
        		   var table = $('#example').DataTable();
        		  
                   var obj = JSON.parse(response);
                   table.clear();
                   var arrayLength = obj.length;
                   for (var i = 0; i < arrayLength; i++) {
                       table.row.add( [obj[i].subject,obj[i].predicate,obj[i].object]);
                   }
                   table.draw();
        	   },
        	   error: function(xhr) {
        		   var table = $('#example').DataTable();
          		  table.clear();
                   table.draw();
                   alert(xhr);
        	   }
        	 });
           
         }
         
         function runQuery(){
        	 if(entity == ""){
        		 alert("Please select a entity");
        	 }else{
        		 var query = document.getElementById("exampleFormControlTextarea1").value;
        	    $.ajax({
              	   url: "LoadValues",
              	   type: "get", //send it through get method
              	   data: { 
              		 entity: entity,
              		hasQuery: "true",
              		query: query
              	    },
              	   success: function(response) {
              		   var table = $('#example').DataTable();
              		  
                         var obj = JSON.parse(response);
                         table.clear();
                         var arrayLength = obj.length;
                         for (var i = 0; i < arrayLength; i++) {
                             table.row.add( [obj[i].subject,obj[i].predicate,obj[i].object]);
                         }
                         table.draw();
              	   },
              	   error: function(xhr) {
              		 var table = $('#example').DataTable();
             		  table.clear();
                      table.draw();
                      alert(xhr);
              	   }
              	 });
        	 }
        	 
         }
      </script>
   </body>
</html>

